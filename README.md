# Image Segmentation
## By-Aarush Kumar

Image Segmentation using skimage and numpy.
In digital image processing and computer vision, image segmentation is the process of partitioning a digital image into multiple segments (sets of pixels, also known as image objects).Image segmentation is typically used to locate objects and boundaries (lines, curves, etc.) in images.Segmentation is an important stage of the image recognition system, because it extracts the objects of our interest, for further processing such as description or recognition.Segmentation techniques are used to isolate the desired object from the image in order to perform analysis of the object.
Thankyou!
