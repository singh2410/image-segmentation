#!/usr/bin/env python
# coding: utf-8

# # Image Segmentation 
# #By- Aarush Kumar
# #Dated: May 26,2021
# 
# ## Image Segmentation:
# With image segmentation, each annotated pixel in an image belongs to a single class. It is often used to label images for applications that require high accuracy and is manually intensive because it requires pixel-level accuracy. A single image can take up to 30 minutes or beyond to complete. The output is a mask that outlines the shape of the object in the image. Although segmentation annotations come in a lot of different types (such as semantic segmentation, instance segmentation, panoptic segmentation, etc), the practice of image segmentation generally describes the need to annotate every pixel of the image with a class. 

# In[1]:


import matplotlib.pyplot as plt
from skimage.io import imread
from skimage import color
import numpy as np


# In[2]:


cimage = imread('https://images.unsplash.com/photo-1580015915218-685fd3cbfa97?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1267&amp;q=80')
fig, ax = plt.subplots(figsize=(20,20))
ax.imshow(cimage)
ax.axis('off')


# In[3]:


# convert the image from RGB to LAB
lab_img = color.rgb2lab(cimage)
x,y,z = lab_img.shape


# In[4]:


# to plot the colors we will use the RGB values from the image directly for colors. 
to_plot = cimage.reshape(x*y, 3)
colors_map = to_plot.astype(np.float)/256


# In[5]:


# create dataset for scatter plot
scatter_x = []
scatter_y = []
for xi in range(x):
    for yi in range(y):
        L_val = lab_img[xi,yi][0] 
        A_val = lab_img[xi,yi][1] 
        B_val = lab_img[xi,yi][2]
        scatter_x.append(A_val)
        scatter_y.append(B_val)
        
plt.figure(figsize=(20,20))
plt.xlabel("a* from green to red")
plt.ylabel("b* from blue to yellow")
plt.scatter(scatter_x,scatter_y, c=colors_map)


# In[6]:


def filter_color(L_val_min, A_val_min, A_val_max, B_val_min, B_val_max):
    filtered_image = np.copy(cimage)
    for xi in range(x):
        for yi in range(y):
            L_val = lab_img[xi,yi][0] 
            A_val = lab_img[xi,yi][1] 
            B_val = lab_img[xi,yi][2]
            if L_val > L_val_min and A_val > A_val_min and A_val < A_val_max  and B_val > B_val_min and B_val < B_val_max:
                pass
            else:
                filtered_image[xi, yi] = [255,255,255]   
    return filtered_image

lab_img = color.rgb2lab(cimage)
yellow = filter_color(70, -50, 0, 30, 100)
red = filter_color(30, 25, 100, 0, 100)
green = filter_color(50, -128, -20, 0, 50)
blue = filter_color(50,-40, 30, -128, -20)
white = filter_color(93, -25, 25, -25, 25)
pink = filter_color(50, 20,128,-50,0)

fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(20,20))
ax[0][0].imshow(pink)
ax[0][0].set_title("pink Candies")
ax[0][0].axis('off')

ax[0][1].imshow(yellow)
ax[0][1].set_title("yellow Candies")
ax[0][1].axis('off')

ax[1][0].imshow(red)
ax[1][0].set_title("red Candies")
ax[1][0].axis('off')

ax[1][1].imshow(green)
ax[1][1].set_title("green Candies")
ax[1][1].axis('off')

ax[2][0].imshow(white)
ax[2][0].set_title("white Candies")
ax[2][0].axis('off')

ax[2][1].imshow(blue)
ax[2][1].set_title("blue Candies")
ax[2][1].axis('off')

